<?php

namespace Redenge\Base\Shop\Presenters;


/**
 * Description of ICartPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICartPresenter
{

	/**
	 * Vrátí id variant/id produktů položek v košíku
	 */
	function getVariantIds();

	/**
	 * Vrátí výslednou sumu košíku s DPH
	 */
	function getTotalPriceVat();
}
