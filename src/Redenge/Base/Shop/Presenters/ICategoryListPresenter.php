<?php

namespace Redenge\Base\Shop\Presenters;

/**
 * Description of ICategoryListPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICategoryListPresenter
{

	function getCategoryId();

	function getVariantIds();

	function getProductNames();

}
