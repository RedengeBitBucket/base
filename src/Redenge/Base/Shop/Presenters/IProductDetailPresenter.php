<?php

namespace Redenge\Base\Shop\Presenters;


/**
 * Description of IProductDetailPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IProductDetailPresenter
{

	/**
	 * Vrátí id varianty nebo id produktu (záleží na konkrétním shopu)
	 */
	function getVariantId();

	/**
	 * Vrátí název produktu
	 */
	function getName();

	/**
	 * Vrátí prodejní cenu produktu s DPH
	 */
	function getPriceVat();

}
