<?php

namespace Redenge\Base\Shop\Presenters;


/**
 * Description of IPurchasePresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IPurchasePresenter
{

	/**
	 * Vrátí id variant/id produktů položek v objednávce
	 */
	function getOrderVariantIds();

	/**
	 * Vrátí položky objednávky
	 */
	function getOrderItems();

	/**
	 * Vrátí výslednou sumu objednávky bez DPH
	 */
	function getOrderTotalPrice();

	/**
	 * Vrátí výslednou sumu objednávky s DPH
	 */
	function getOrderTotalPriceVat();

	/**
	 * Vrátí výslednou sumu za dopravu a platbu bez DPH
	 */
	function getOrderShippingPrice();

	/**
	 * Vrátí jména produktů
	 */
	function getOrderProductNames();

	/**
	 * Vrátí ISO kód měny
	 */
	function getCurrency();

	/**
	 * Vrátí číslo (index) objednávky
	 */
	function getOrderNumber();

}
