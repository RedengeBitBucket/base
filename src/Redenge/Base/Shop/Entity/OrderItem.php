<?php

namespace Redenge\Base\Shop\Entity;


/**
 * Description of OrderItem
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class OrderItem
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $sku;

	/**
	 * @var string
	 */
	private $category;

	/**
	 * @var float
	 */
	private $price;

	/**
	 * @var float
	 */
	private $quantity;


	public function getName()
	{
		return $this->name;
	}


	public function getSku()
	{
		return $this->sku;
	}


	public function getCategory()
	{
		return $this->category;
	}


	public function getPrice()
	{
		return $this->price;
	}


	public function getQuantity()
	{
		return $this->quantity;
	}


	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}


	public function setSku($sku)
	{
		$this->sku = $sku;
		return $this;
	}


	public function setCategory($category)
	{
		$this->category = $category;
		return $this;
	}


	public function setPrice($price)
	{
		$this->price = $price;
		return $this;
	}


	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
		return $this;
	}

}
