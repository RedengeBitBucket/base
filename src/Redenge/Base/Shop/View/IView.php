<?php

namespace Redenge\Base\Shop\View;


/**
 * Description of IView
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IView
{

	/**
	 * @param string $name
	 * @param string $value
	 */
	function addToView($name, $value);

}
