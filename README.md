# Redenge Base
Jedná se o základní balíček, který se snaží sjednotit jednotlivé verze Redenge shopu. 

Obsahuje balíček kdyby/events, který vychází z návrhového vzoru ,,Pozorovatel". Hlavní předností je ,,chytrá" komunikace mezi jednotlivými moduly e-shopu.

Součastí balíčku jsou interface třídy pro jednotlivé presentery/moduly, jako jsou např. Cart, Purchase.

## Požadavky
Redenge/Base requires PHP 5.6 or higher.

- [kdyby/events](https://github.com/Kdyby/Events)


## Instalace
The best way to install Redenge/Base is using  [Composer](http://getcomposer.org/):
1) You must add repository field to your composer.json
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```
2) Require base package
```sh
$ composer require redenge/base
```

### Instalace pro eshopy, které běží na ,,Nette frameworku"
1) Vložení extensions pro balík kdyby/events
```yml
extensions:
	events: Kdyby\Events\DI\EventsExtension
```

## Použití

